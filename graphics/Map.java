import java.awt.Graphics;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.Random;

public class Map {

    private final int TILE_PATH = 0;
    private final int TILE_EMPTY = 1;

    private final int TILE_WIDTH = 20;
    private final int TILE_HEIGHT = 20;
    private final int TILE_SEPARATION = 10;

    private int[][] map;
    private int rows;
    private int columns;

    private int currentRow;
    private int currentColumn;

    public Map(int rows, int columns) {
        map = new int[rows][columns];
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                map[row][column] = 0;
            }
        }

        this.rows = rows;
        this.columns = columns;

        currentColumn = getRandomBetweenRange(0, columns - 1);
        currentRow = getRandomBetweenRange(0, rows - 1);
    }

    public int getWidth() {
        return (columns * TILE_WIDTH) + ((columns - 1) * TILE_SEPARATION);
    }

    public int getHeight() {
        return (rows * TILE_HEIGHT) + ((rows - 1) * TILE_SEPARATION);
    }

    public void draw(Graphics g, int x, int y) {

        int offsetX = 0;
        int offsetY = 0;

        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                Color tileColor = getColorFromTile(map[row][column]);
                if (row == currentRow && column == currentColumn) {
                    tileColor = Color.red;
                }

                g.setColor(tileColor);
                g.fillRect(x + offsetX, y + offsetY, TILE_WIDTH, TILE_HEIGHT);

                offsetX += (TILE_SEPARATION + TILE_WIDTH);
            }
            offsetX = 0;
            offsetY += (TILE_SEPARATION + TILE_HEIGHT);
        }
    }

    public void deleteRandomTile() {
        int randomRow = getRandomBetweenRange(0, rows - 1);
        int randomColumn = getRandomBetweenRange(0, columns - 1);
        map[randomRow][randomColumn] = TILE_EMPTY;
    }

    public int getRandomBetweenRange(int min, int max) {
        return (new Random().nextInt(max - min + 1) + min);
    }

    private Color getColorFromTile(int value) {
        switch (value) {
            case TILE_PATH:
                return Color.WHITE;

            case TILE_EMPTY:
                return Color.BLACK;
        }
        return Color.BLACK;
    }

    public void update(int keyCode) {

        int targetRow = currentRow;
        int targetColumn = currentColumn;

        if (keyCode == KeyEvent.VK_UP) {
            if (targetRow > 0) {
                targetRow--;
            }
        } else if (keyCode == KeyEvent.VK_DOWN) {
            if (targetRow < (rows - 1)) {
                targetRow++;
            }
        } else if (keyCode == KeyEvent.VK_LEFT) {
            if (targetColumn > 0) {
                targetColumn--;
            }
        } else if (keyCode == KeyEvent.VK_RIGHT) {
            if (targetColumn < (columns - 1)) {
                targetColumn++;
            }
        } else if (keyCode == KeyEvent.VK_SPACE) {
            deleteRandomTile();
        }

        if (map[targetRow][targetColumn] == TILE_PATH) {
            currentRow = targetRow;
            currentColumn = targetColumn;
        }
    }
}