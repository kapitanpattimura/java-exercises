import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MainWindow extends JFrame implements ActionListener, KeyListener {

    public static final int WINDOW_WIDTH = 500;
    public static final int WINDOW_HEIGHT = 500;

    private JPanel buttonPanel;
    private DrawingPanel drawingPanel;
    private JButton buttonUpdate;

    private Map map;

    public static void main(String[] args) {
        MainWindow main = new MainWindow();
//        main.pack();
        main.setVisible(true);
    }

    public MainWindow() {
        initializeGUI();
        initializeEvents();
        initializeMap();
    }

    private void initializeGUI() {
        setTitle("Graphics Test");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());
        setResizable(false);

        drawingPanel = new DrawingPanel();
        add(drawingPanel, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel();
        buttonUpdate = new JButton();
        buttonUpdate.setText("Nuke!");
        buttonUpdate.addActionListener(this);
        buttonUpdate.setFocusable(false);
        buttonPanel.add(buttonUpdate);

        add(buttonPanel, BorderLayout.SOUTH);
    }

    private void initializeEvents() {
        setFocusable(true);
        addKeyListener(this);
    }

    private void initializeMap() {
        map = new Map(10, 10);
        drawingPanel.setMap(map);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == buttonUpdate) {
            map.deleteRandomTile();
            drawingPanel.repaint();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {}

    @Override
    public void keyReleased(KeyEvent e) {
        map.update(e.getKeyCode());
        drawingPanel.repaint();
    }
}