import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;

public class DrawingPanel extends JPanel {

    private Map map;

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getSize().width, getSize().height);

        if (map != null) {
            int x = (getSize().width / 2)- (map.getWidth() / 2);
            int y = (getSize().height / 2) - (map.getHeight() / 2);
            map.draw(g, x, y);
        }
    }

    public void setMap(Map map) {
        this.map = map;
    }

}
