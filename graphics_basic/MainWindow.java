import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.Color;

public class MainWindow extends JFrame implements ActionListener, KeyListener {

	private static final int WINDOW_WIDTH = 500;
	private static final int WINDOW_HEIGHT = 500;

	private JButton updateButton;
	private DrawingPanel drawingPanel;
	private Map map;
	
	public static void main(String[] args) {
		MainWindow mainWindow = new MainWindow();
		mainWindow.setVisible(true);
		System.out.println("Running!");
	}

	public MainWindow() {
		initializeGUI();
		initializeEvents();
		initializeMap();
	}

	private void initializeGUI() {
		setTitle("Graphics Test");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		setLayout(new BorderLayout());

		drawingPanel = new DrawingPanel();
		add(drawingPanel, BorderLayout.CENTER);

		JPanel buttonsPanel = new JPanel();
		updateButton = new JButton();
		updateButton.setText("Update");
		buttonsPanel.add(updateButton);		
		add(buttonsPanel, BorderLayout.SOUTH);
	}

	private void initializeEvents() {
		updateButton.addActionListener(this);
		addKeyListener(this);
		setFocusable(true); // in order to make KeyListener work!
	}

	private void initializeMap() {
		map = new Map(5, 5);
		drawingPanel.setMap(map);
	}

	public void actionPerformed(ActionEvent e) {		
		if (e.getSource() == updateButton) {
			System.out.println("Action happened!");
			drawingPanel.updateColor(Color.BLUE);
		}
	}

	public void keyReleased(KeyEvent e) {
		if (map != null) {
			map.update(e.getKeyCode());
			//drawingPanel.repaint();
		}
	}

	public void keyPressed(KeyEvent e) {
		//System.out.println("keyPressed=" + e.getKeyCode());
	}

	public void keyTyped(KeyEvent e) {
		//System.out.println("keyTyped=" + e.getKeyCode());
	}

}