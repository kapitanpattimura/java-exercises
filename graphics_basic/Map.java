import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;

public class Map {

	private final int TILE_PATH = 0;
	private final int TILE_HOLE = 1;
	private final int TILE_BOX = 2;
	private final int TILE_DOOR = 3;

	private final int TILE_WIDTH = 30;
	private final int TILE_HEIGHT = 30;
	private final int TILE_SEPARATION = 5;

	private final int CHARACTER_SIZE = TILE_WIDTH / 2;

	private int rows;
	private int columns;

	private int[][] map;

	private int characterColumn = 0;
	private int characterRow = 0;

	public Map(int rows, int columns) {
		this.rows = rows;		
		this.columns = columns;

		map = new int[rows][columns];

		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				map[row][column] = TILE_PATH;
				/*
				if (row == 1) {
					map[row][column] = TILE_HOLE;
				}*/
			}
		}
	}

	public int getWidth() {
		return (columns * TILE_WIDTH) + ((columns - 1) * TILE_SEPARATION);
	}

	public int getHeight() {
		return (rows * TILE_HEIGHT) + ((rows - 1) * TILE_SEPARATION);
	}

	public void update(int keyCode) {
		if (keyCode == KeyEvent.VK_RIGHT) {
			characterColumn++;
		}
	}

	public void draw(Graphics g, int x, int y) {
		//g.setColor(Color.RED);
		//g.fillRect(x, y, 100, 100);

		int offsetX = 0;
		int offsetY = 0;

		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				Color color = getTileColor(map[row][column]);
				g.setColor(color);
				g.fillRect(x + offsetX, y + offsetY, TILE_WIDTH, TILE_HEIGHT);				

				if (row == characterRow && column == characterColumn) {
					g.setColor(Color.GREEN);				
					int characterX = x + offsetX + (TILE_WIDTH / 2) - (CHARACTER_SIZE / 2);
					int characterY = y + offsetY + (TILE_HEIGHT / 2) - (CHARACTER_SIZE / 2);
					g.fillRect(characterX, characterY, CHARACTER_SIZE, CHARACTER_SIZE);
				}

				offsetX += TILE_WIDTH + TILE_SEPARATION;
			}
			offsetX = 0;
			offsetY += TILE_HEIGHT + TILE_SEPARATION;
		}
	}

	private Color getTileColor(int tileValue) {
		switch (tileValue) {
			case TILE_PATH:
				return Color.WHITE;
			case TILE_HOLE:
				return Color.RED;			
		}
		return Color.BLUE;
	}
}